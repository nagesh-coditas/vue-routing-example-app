import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Jobs from '../views/jobs/Jobs.vue';
import JobDetails from '../views/jobs/JobDetails.vue';
import NotFound404 from '../components/NotFound404.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path:"/Jobs",
    name:"Jobs",
    component:Jobs
  },
  {
    path:"/Jobs/:id",
    name:'JobDetails',
    component:JobDetails,
    props:true
  },
  { // 404 not found path
    path:'/:catchAll(.*)',
    name:'NotFound404',
    component:NotFound404,
  },
  { // redirect
    path:'/alljobs',
    redirect:'Jobs'
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
